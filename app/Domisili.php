<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Domisili extends Model
{
    protected $fillable = [
        'id',
        'nik',
        'petugas_id',
        'keterangan',
    ];
    protected $table = 'domisili';

    public function penduduk()
    {
        return $this->belongsToMany('App\Penduduk', 'domisili', 'id', 'nik',);
    }
    public function petugas()
    {
        return $this->belongsToMany('App\Petugas', 'domisili', 'id', 'petugas_id');
    }
    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->translatedFormat('d F Y');
    }
}
