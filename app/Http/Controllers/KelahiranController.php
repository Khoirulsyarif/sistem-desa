<?php

namespace App\Http\Controllers;

use Alert;
use App\Kelahiran;
use App\Petugas;
use App\Penduduk;
use Illuminate\Http\Request;

class KelahiranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kelahiran = Kelahiran::all();
        return view('kelahiran.index', compact('kelahiran'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $petugas = Petugas::all();
        $penduduk = Penduduk::all();
        return view('kelahiran.create', compact('petugas', 'penduduk'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $kelahiran = kelahiran::create([

            'nama' => $request->nama,
            'nik_ayah' => $request->nik_ayah,
            'nik_ibu' => $request->nik_ibu,
            'petugas_id' => $request->petugas_id,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'jam' => $request->jam,
            'jk' => $request->jk,

        ]);
        Alert::success('Success', 'Data Kelahiran berhasil disimpan');
        return redirect()->route('kelahiran.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $penduduk = Penduduk::all();
        $petugas = Petugas::all();
        $kelahiran = Kelahiran::findorfail($id);
        $kelahiran = Kelahiran::Find($id);
        return view('kelahiran.show', compact('kelahiran', 'penduduk', 'petugas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $penduduk = Penduduk::all();
        $petugas = Petugas::all();
        $kelahiran = kelahiran::findorfail($id);
        return view('kelahiran.edit', compact('kelahiran', 'penduduk', 'petugas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kelahiran_data = [
            'nama' => $request->nama,
            'nik_ayah' => $request->nik_ayah,
            'nik_ibu' => $request->nik_ibu,
            'petugas_id' => $request->petugas_id,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->name,
            'jam' => $request->jam,
            'jk' => $request->jk,
            'tgl' => $request->tgl,
        ];
        Kelahiran::whereId($id)->update($kelahiran_data);
        Alert::success('Success', 'Data Kelahiran berhasil diedit');
        return redirect()->route('kelahiran.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kelahiran = Kelahiran::findorfail($id);
        $kelahiran->delete();
        Alert::success('Success', 'Data Kelahiran berhasil dihapus');
        return redirect()->route('kelahiran.index');
    }
}
