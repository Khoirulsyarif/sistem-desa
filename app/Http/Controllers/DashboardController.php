<?php

namespace App\Http\Controllers;

use App\Penduduk;
use App\Pendidikan;
use App\Petugas;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $penduduk = Penduduk::all();
        $petugas = Petugas::all();
        return view('dashboard', compact('penduduk', 'petugas'));
    }
}
