<?php

namespace App\Http\Controllers;

// use RealRashid\SweetAlert\Facades\Alert;
use Alert;
use App\Domisili;
use App\Petugas;
use App\Penduduk;
use Illuminate\Http\Request;


class DomisiliController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $domisili = Domisili::all();
        $penduduk = Penduduk::all();
        $petugas = Petugas::all();
        return view('domisili.index', compact('domisili', 'penduduk', 'petugas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $petugas = Petugas::all();
        $penduduk = Penduduk::all();
        return view('domisili.create', compact('petugas', 'penduduk'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $domisili = Domisili::create([
            'nik' => $request->nik,
            'keterangan' => $request->keterangan,
            'petugas_id' => $request->petugas_id,
        ]);
        Alert::success('Success', 'Data Domisili berhasil disimpan');
        return redirect()->route('domisili.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $penduduk = Penduduk::all();
        $petugas = Petugas::all();
        $domisili = Domisili::findorfail($id);
        $domisili = Domisili::Find($id);
        return view('domisili.show', compact('domisili', 'penduduk', 'petugas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $penduduk = Penduduk::all();
        $petugas = Petugas::all();
        $domisili = Domisili::findorfail($id);
        return view('domisili.edit', compact('domisili', 'penduduk', 'petugas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $domisili_data = [
            'nik' => $request->nik,
            'keterangan' => $request->keterangan,
            'petugas_id' => $request->petugas_id,
        ];
        Domisili::whereId($id)->update($domisili_data);
        Alert::success('Success', 'Data Domisili berhasil diedit');
        return redirect()->route('domisili.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $domisili = Domisili::findorfail($id);
        $domisili->delete();
        return redirect()->route('domisili.index')->with('success', 'Data Domisili berhasil dihapus');
    }
}
