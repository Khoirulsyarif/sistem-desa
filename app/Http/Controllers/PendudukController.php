<?php

namespace App\Http\Controllers;

use Alert;
use App\Penduduk;
use App\Pendidikan;
use Illuminate\Http\Request;

class PendudukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penduduk = Penduduk::all();
        return view('penduduk.index', compact('penduduk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pendidikan = Pendidikan::all();
        return view('penduduk.create', compact('pendidikan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $penduduk = Penduduk::create([
            'nik' => $request->nik,
            'no_kk' => $request->no_kk,
            'name' => $request->name,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'jk' => $request->jk,
            'alamat' => $request->alamat,
            'status' => $request->status,
            'pend_id' => $request->pend_id,
            'pekerjaan' => $request->pekerjaan,
            'agama' => $request->agama,

        ]);
        Alert::success('Success', 'Data Penduduk berhasil disimpan');
        return redirect()->route('penduduk.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $penduduk = Penduduk::findorfail($id);
        return view('penduduk.show', compact('penduduk'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pendidikan = Pendidikan::all();
        $penduduk = Penduduk::findorfail($id);
        return view('penduduk.edit', compact('penduduk', 'pendidikan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $penduduk_data = [
            'nik' => $request->nik,
            'no_kk' => $request->no_kk,
            'name' => $request->name,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'jk' => $request->jk,
            'alamat' => $request->alamat,
            'status' => $request->status,
            'pend_id' => $request->pend_id,
            'pekerjaan' => $request->pekerjaan,
            'agama' => $request->agama,
        ];
        Penduduk::whereId($id)->update($penduduk_data);
        Alert::success('Success', 'Data Penduduk berhasil diedit');
        return redirect()->route('penduduk.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $penduduk = Penduduk::findorfail($id);
        $penduduk->delete();
        Alert::success('Success', 'Data Penduduk berhasil dihapus');
        return redirect()->route('penduduk.index');
    }
}
