<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Kelahiran extends Model
{

    protected $fillable = [
        'id',
        'nama',
        'nik_ayah',
        'nik_ibu',
        'petugas_id',
        'tempat_lahir',
        'tgl_lahir',
        'jam',
        'jk',
    ];
    protected $table = 'kelahiran';

    public function penduduk()
    {
        return $this->belongsToMany('App\Penduduk', 'kelahiran', 'id', 'nik_ayah',);
    }
    public function pendudukk()
    {
        return $this->belongsToMany('App\Penduduk', 'kelahiran', 'id', 'nik_ibu',);
    }
    public function petugas()
    {
        return $this->belongsToMany('App\Petugas', 'kelahiran', 'id', 'petugas_id');
    }
    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->translatedFormat('d F Y');
    }
}
