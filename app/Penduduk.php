<?php

namespace App;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;

class Penduduk extends Model
{
    protected $fillable = [
        'id',
        'nik',
        'no_kk',
        'name',
        'tempat_lahir',
        'tgl_lahir',
        'jk',
        'alamat',
        'status',
        'pend_id',
        'pekerjaan',
        'agama',
    ];
    protected $table = 'penduduk';
}
