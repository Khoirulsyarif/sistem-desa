<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">

    <title>Sistem Informasi Desa</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

    <!-- Styles -->
    <style>
        html,
        body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links>a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>

<body>
    <div class="flex-center position-ref full-height">
        @if (Route::has('login'))
        <div class="top-right links">
            @auth
            <a href="{{ url('/home') }}">Home</a>
            @else
            <a href="{{ route('login') }}">Login</a>

            @if (Route::has('register'))
            <a href="{{ route('register') }}">Register</a>
            @endif
            @endauth
        </div>
        @endif

        {{-- <div class="content">
            <div class="title m-b-md">
                Laravel
            </div>
        </div> --}}

        <div class="content-wrapper">
            <section class="content">
                <div class="box box-info">
                    <div class="box-header">
                        <h4 style="text-align:center"><b>SELAMAT DATANG DI SISTEM INFORMASI PENGELOLAAN DATA
                                PENDUDUK<br>KANTOR
                                KEPALA DESA WARUNG BAMBU</b></h4>
                        <hr>
                    </div>
                    <div style="text-align: center;"> <img style="cent" src="{{ asset('img/logo-karawang.png') }}"
                            width="200" style="margin-right:50px"> </div>


                    <br>
                    <br>
                    <section>
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4 style="text-align:center"> <b> VISI DAN MISI </b></h4>
                                    <hr>
                                </div>
                            </div>

                            <div class="row">
                                <div style="text-align: left;" class="col-sm-5 col-sm-offset-1">
                                    <p> Desa Warungbambu berkarya serta membangun untuk menciptakan masyarakat yang
                                        mandiri
                                        dan harmonis</p>
                                </div>

                                <div style="text-align: left;" class="col-sm-5 col-sm-offset-1">
                                    <p> 1.Meningkatkan pelayanan masyarakat di semua bidang <br>
                                        2.Penguatan dan peningkatan taraf hidup ekonomi masyarakat <br>
                                        3.Meningkatkan dan memperbaiki tata kelola pemerintahan desa
                                    </p>
                                </div>
                            </div>
                        </div>
                    </section>

                    </center>
                </div>
            </section>
        </div>
    </div>
</body>

</html>