@extends('layout.home')
@push('style')
<link rel="stylesheet" href="{{ asset('admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush
@section('title')
Tambah Surat Domisili
@endsection
@section('content')
<label class="col-md-6">
  <form action="{{ route('domisili.store') }}" method="post">
    @csrf
    <div class="card-body">
      <a href="{{ route('penduduk.create') }}" class="btn btn-sm btn-primary pull-right">Tambah
        Penduduk</a><br /><br />

      <div class="form-group">
        <div class="form-group">
          <label>NIK</label>
          <select name="nik" class="form-control" required>
            <option value="" selected disabled>- pilih -</option>
            @foreach ($penduduk as $item)
            <option value="{{ $item->id }}" {{ old('nik')==$item->nik ? 'selected' : null }}>{{ $item->nik }} -
              {{ $item->name }}

            </option>
            @endforeach
        </div>
        </select>
      </div>
      <div class="form-group">
        <label>Keterangan</label>
        <textarea name="keterangan" class="form-control" rows="3" required></textarea>
      </div>
      <div class="form-group">
        <div class="form-group">
          <label>Tanda Tangan</label>
          <select name="petugas_id" class="form-control" required>
            <option value="" selected disabled>- pilih -</option>
            @foreach ($petugas as $item)
            <option value="{{ $item->id }}" {{ old('petugas_id')==$item->id ? 'selected' : null }}>{{ $item->nama }}
            </option>
            @endforeach
        </div>
        </select>
      </div>
      <div class="form-group">
        <input type="submit" class="btn btn-success">
        <a href="" class="btn btn-danger">Batal</a>
      </div>
    </div>
  </form>
</label>
@endsection