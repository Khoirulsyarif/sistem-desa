<style type="text/css">
  @media print and (width: 21cm) and (height: 33cm) {
    @page {
      margin: 1cm;
    }
  }

  .tabelku {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 2px;
  }
</style>
<br /><br /><br />
<center>
  <font size="5"><u><b>SURAT KETERANGAN DOMISILI</b></u></font><br /> <br> Nomor:
  145/ /IV/DS/{{ substr($domisili->created_at, 10, 14) }}
</center>
<br /><br /><br />
<font align="justify">
  Yang bertandatangan di bawah ini , Kepala Desa Warungbambu Kecamatan Karawang Timur Kabupaten Karawang menerangkan :
</font>
<table width="100%">
  <tr>
    <td width="20%">Nama Lengkap</td>
    <td width="3%">:</td>
    <td width="77%">
      @foreach ($domisili->penduduk as $item)
      {{ $item->name }}
      @endforeach
    </td>
  </tr>
  <tr>
    <td>NIK</td>
    <td>:</td>
    <td>
      @foreach ($domisili->penduduk as $item)
      {{ $item->nik }}
      @endforeach
    </td>
  </tr>
  <tr>
    <td>Tempat/Tanggal Lahir</td>
    <td>:</td>
    <td> @foreach ($domisili->penduduk as $item)
      {{ $item->tempat_lahir }}
      @endforeach / @foreach ($domisili->penduduk as $item)
      {{ date('d F Y', strtotime($item->tgl_lahir)) }}
      @endforeach
    </td>
  </tr>
  <tr>
    <td>Jenis Kelamin</td>
    <td>:</td>
    <td>
      @foreach ($domisili->penduduk as $item)
      {{ $item->jk }}
      @endforeach
    </td>
  </tr>
  <tr>
    <td>Status Perkawinan</td>
    <td>:</td>
    <td>
      @foreach ($domisili->penduduk as $item)
      {{ $item->status }}
      @endforeach
    </td>
  </tr>
  <tr>
    <td>Pekerjaan</td>
    <td>:</td>
    <td>
      @foreach ($domisili->penduduk as $item)
      {{ $item->pekerjaan }}
      @endforeach
    </td>
  </tr>
  <tr>
    <td>Alamat KTP</td>
    <td>:</td>
    <td>
      @foreach ($domisili->penduduk as $item)
      {{ $item->alamat }}
      @endforeach
    </td>
  </tr>
</table>
<font align="justify">
  Berdasarkan Surat pengantar RT/RW setempat, Bahwa yang bersangkutan benar
  saat ini Berdomisili/Bertempat Tinggal di
  @foreach ($domisili->penduduk as $item)
  {{ $item->alamat }}
  @endforeach
  <br /><br />
  Surat Keterangan ini berlaku: {{ $domisili->created_at }} s/d
  {{ date('d F Y', strtotime('+1 month', strtotime($domisili->created_at))) }}
  <br /><br />
  Demikian Surat Keterangan ini dibuat dengan sebenarnya, untuk dapat dipergunakan sesuai keperluannya serta agar yang
  berkepentingan menjadi maklum.<br /><br /><br />
</font>
<table width="100%">
  <tr>
    <td width="50%"></td>
    <td width="50%">
      <center>Warungbambu, {{ $domisili->created_at }} </center>
    </td>
  </tr>
  <tr>
    <td>
      <center>Yang Bersangkutan</center>
    </td>
    <td>
      <center>Kepala Desa Warungbambu</center>
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>
      <center><b><u>
            @foreach ($domisili->penduduk as $item)
            {{ $item->name }}
            @endforeach
          </u></b></center>
    </td>
    <td>
      <center><b><u>
            @foreach ($domisili->petugas as $item)
            {{ $item->nama }}
            @endforeach
          </u></b></center>
    </td>
  </tr>
  <tr>
    <td></td>
    <td>
      <center>NIP. @foreach ($domisili->petugas as $item)
        {{ $item->nip }}
        @endforeach</center>
    </td>
  </tr>
</table>
<script>
  window.print();
</script>