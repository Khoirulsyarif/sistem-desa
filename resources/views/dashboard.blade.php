@extends('layout.home')
@push('style')
<link rel="stylesheet" href="{{ asset('admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush
@section('title')
Dashboard
@endsection
@section('content')

<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="info-box bg-aqua">
    <span class="info-box-icon"> <i class="fa fa-users"></i></span>

    <div class="info-box-content">
      <span class="info-box-text">Penduduk</span>
      <span class="info-box-number"></span>
    </div>
  </div>
</div>
<!-- Petugas -->
<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="info-box bg-green">
    <span class="info-box-icon"><i class="fa fa-user"></i></span>

    <div class="info-box-content">
      <span class="info-box-text"> Data Petugas </span>
      <span class="info-box-number"></span>
    </div>
  </div>
</div>
<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="info-box bg-red">
    <span class="info-box-icon"><i class="fa fa-user"></i></span>

    <div class="info-box-content">
      <span class="info-box-text"> Data Surat </span>
      <span class="info-box-number"></span>
    </div>
  </div>
</div>
</div>
</div>
</section>
</div>
@endsection