<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li>
        <a href="/dashboard">
          <i class="fa fa-circle-o"></i> <span>Dashboard</span>
          <span class="pull-right-container">
          </span>
        </a>
      </li>

      <li class="treeview">
        <a href="">
          <i class="fa fa-envelope-o"></i>
          <span>Master</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li>
            <a href="{{ route('penduduk.index') }}">
              <i class="fa fa-users"></i> <span>Data Penduduk</span>
              <span class="pull-right-container">
              </span>
            </a>
          </li>
          <li>
            <a href="{{ route('petugas.index') }}">
              <i class="fa fa-user"></i> <span>Petugas Desa</span>
              <span class="pull-right-container">
              </span>
            </a>
          </li>
          <li>
            <a href="{{ route('pendidikan.index') }}">
              <i class="fa fa-user"></i> <span>Data Pendidikan</span>
              <span class="pull-right-container">
              </span>
            </a>
          </li>
        </ul>
      </li>
      <li class="treeview">
        <a href="">
          <i class="fa fa-envelope-o"></i>
          <span>Layanan Surat</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ route('kelahiran.index') }}"><i class="fa fa-envelope-o"></i> <span>Surat
                Kelahiran</span></a></li>
          <li><a href="{{ route('domisili.index') }}"><i class="fa fa-envelope-o"></i>
              <span>Surat Domisili</span></a></li>
      </li>
    </ul>
    <li>
      <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                               document.getElementById('logout-form').submit();">
        {{ __('Logout') }}
      </a>

      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
      </form>
      </a>
    </li>
  </section>
  <!-- /.sidebar -->
</aside>
<!-- =============================================== -->
<div class="content-wrapper">