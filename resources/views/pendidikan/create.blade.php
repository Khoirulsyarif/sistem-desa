@extends('layout.home')
@push('style')
<link rel="stylesheet" href="{{ asset('admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush
@section('title')
Tambah Pendidikan
@endsection
@section('content')
<label class="col-md-6">
  <form action="{{ route('pendidikan.store') }}" method="post">
    @csrf
    <div class="form-group">
      <label>Pendidikan</label>
      <input type="text" name="pendidikan" class="form-control" required />
    </div>
    <div class="form-group">
      <button type="submit" class="btn btn-primary">Simpan</button>
      <a href="" class="btn btn-danger">Batal</a>
    </div>
  </form>
</label>
@endsection