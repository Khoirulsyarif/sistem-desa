@extends('layout.home')
@push('style')
<link rel="stylesheet" href="{{ asset('admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush
@section('title')
Data Pendidikan
@endsection
@section('content')

<div class="card">
  <!-- /.card-header -->
  <div class="card-body">
    <br>
    @if (Session::has('success'))
    <div class="alert alert-info alert-dismissible">
      <h4><i class="icon fa fa-info"></i> Sukses!</h4>
      {{ Session('success') }}
    </div>
    @endif
    <a href="{{ route('pendidikan.create') }}" class="btn btn-success btn-sm">Tambah Pendidikan</a> <br> <br>
    <table id="example1" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>No</th>
          <th>Pendidikan</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php $no = 0;?>
        @foreach ($pendidikan as $result )
        <?php $no++ ;?>
        <tr>
          <td>{{ $no }}</td>
          <td>
            {{ $result->pendidikan }}
          </td>
          <td>
            <form action="{{ route('pendidikan.destroy', $result->id) }}" method="POST">
              @csrf
              @method('delete')
              <a href="{{ route('pendidikan.edit', $result->id) }}" class="btn btn-primary">Edit</a>
              <button type="submit" class="btn btn-danger">Hapus</button>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>

  </div>
  @push('script')
  <script src="{{ asset('admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{ asset('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script>
    $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
  </script>
  @endpush
  @endsection