@extends('layout.home')
@push('style')
<link rel="stylesheet" href="{{ asset('admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush
@section('title')
Data Penduduk
@endsection
@section('content')

<div class="card">
  <!-- /.card-header -->
  <div class="card-body">
    <table id="example1" class="table table-bordered table-striped">

      <tr>
        <th>No Kartu Keluarga : </th>
        <td>{{ $penduduk->no_kk }}</td>
      </tr>
      <tr>
        <th>Nama : </th>
        <td>{{ $penduduk->name }}</td>
      </tr>
      <tr>
        <th>Tempat Tangal Lahir : </th>
        <td>{{ $penduduk->tempat_lahir }} , {{ $penduduk->tgl_lahir }}</td>
      </tr>
      <tr>
        <th>Jenis Kelamin : </th>
        <td>{{ $penduduk->jk }}</td>
      </tr>
      <tr>
        <th>Alamat : </th>
        <td>{{ $penduduk->alamat }}</td>
      </tr>
      <tr>
        <th>pendididkan : </th>
        <td>{{ $penduduk->pendidikan }}</td>
      </tr>
      <tr>
        <th>Pekerjaan : </th>
        <td>{{ $penduduk->pekerjaan }}</td>
      </tr>
      <tr>
        <th>Agama : </th>
        <td>{{ $penduduk->agama }}</td>
      </tr>
      <tr>
        <th>Status Pernikahan : </th>
        <td>{{ $penduduk->ststus }}</td>
      </tr>
    </table>
    <button onClick="goBack()" .GoBack class="btn btn-danger"> Kembali</button>

  </div>
  @push('script')
  <script src="{{ asset('admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}">
  </script>
  <script src="{{ asset('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script>
    $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
  </script>
  @endpush
  @endsection