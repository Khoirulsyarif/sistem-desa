@extends('layout.home')
@push('style')
<link rel="stylesheet" href="{{ asset('admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush
@section('title')
Data Penduduk
@endsection
@section('content')

<div class="card">
  <!-- /.card-header -->
  <div class="card-body">
    <br>
    @if (Session::has('success'))
    <div class="alert alert-info alert-dismissible">
      <h4><i class="icon fa fa-info"></i> Sukses!</h4>
      {{ Session('success') }}
    </div>
    @endif
    <a href="{{ route('penduduk.create') }}" class="btn btn-success btn-sm">Tambah Penduduk</a> <br> <br>
    <table id="example1" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>No</th>
          <th>NIK</th>
          <th>Nama</th>
          <th>Tanggal Lahir</th>
          <th>Jenis Kelamin</th>
          <th>Alamat</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php $no = 0;?>
        @foreach ($penduduk as $result )
        <?php $no++ ;?>
        <tr>
          <td>{{ $no }}</td>
          <td>
            {{ $result->nik }}
          </td>
          <td>
            {{ $result->name }}
          </td>
          <td>
            {{ date('d F Y', strtotime($result->tgl_lahir)) }}
          </td>
          <td>
            {{ $result->jk }}
          </td>
          <td>
            {{ $result->alamat }}
          </td>
          <td>
            <form action="{{ route('penduduk.destroy', $result->id) }}" method="POST">
              @csrf
              @method('delete')
              <a href="{{ route('penduduk.edit', $result->id) }}" class="btn btn-primary">Edit</a>
              <button type="submit" class="btn btn-danger">Hapus</button>
              <a href="{{ route('penduduk.show',$result->id) }}" class="btn btn-warning">Lihat</a>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>

  </div>
  @push('script')
  <script src="{{ asset('admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{ asset('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script>
    $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
  </script>
  @endpush
  @endsection