@extends('layout.home')
@push('style')
<link rel="stylesheet" href="{{ asset('admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush
@section('title')
Tambah Penduduk
@endsection
@section('content')
<label class="col-md-6">
  <form action="{{ route('penduduk.store') }}" method="post">
    @csrf
    <div class="form-group">
      <label>NIK</label>
      <input type="text" name="nik" class="form-control" required />
    </div>

    <div class="form-group">
      <label>No Kartu Keluarga</label>
      <input type="text" name="no_kk" class="form-control" required />
    </div>

    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="name" class="form-control" required />
    </div>

    <div class="form-group">
      <label>Tempat Tanggal Lahir</label>
      <div class="row">
        <div class="col-xs-3">
          <input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat">
        </div>
        <div class="col-xs-5">
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="date" name="tgl_lahir" class="form-control pull-right" id="" placeholder="Tanggal Lahir">
          </div>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label>Jenis Kelamin</label>
      <select name="jk" class="form-control" required>
        <option value="" selected disabled>- pilih -</option>
        <option value="Laki Laki">Laki Laki</option>
        <option value="Perempuan">Perempuan</option>
      </select>
    </div>
    <div class="form-group">
      <label>Alamat</label>
      <textarea name="alamat" class="form-control" rows="3" required></textarea>
    </div>
    <div class="form-group">
      <label>Agama</label>
      <select class="form-control selectlive" name="agama" required>
        <option value="" selected disabled>- pilih -</option>
        <option value="Islam">Islam</option>
        <option value="Kristen">Kristen</option>
        <option value="Katholik">Katholik</option>
        <option value="Hindu">Hindu</option>
        <option value="Budha">Budha</option>
        <option value="Konghucu">Konghucu</option>
      </select>
    </div>
    <div class="form-group">
      <label>Pekerjaan</label>
      <input type="text" name="pekerjaan" class="form-control" required />
    </div>
    <div class="form-group">
      <div class="form-group">
        <label>Pendidikan</label>
        <select name="pend_id" class="form-control" required>
          <option value="" selected disabled>- pilih -</option>
          @foreach ($pendidikan as $item)
          <option value="{{ $item->id }}" {{ old('pend_id')==$item->id ? 'selected' : null }}>{{ $item->pendidikan }}
          </option>
          @endforeach
      </div>
      </select>
    </div>
    <div class="form-group">
      <label>Status Perkawinan</label>
      <select name="status" class="form-control" required>
        <option value="" selected disabled>- pilih -</option>
        <option value="Menikah">Menikah</option>
        <option value="Belum Menikah">Belum Menikah</option>
      </select>
    </div>
    <div class="form-group">
      <button type="submit" class="btn btn-primary">Simpan</button>
      <a href="" class="btn btn-danger">Batal</a>
    </div>
  </form>
</label>
@endsection