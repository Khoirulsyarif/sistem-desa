@extends('layout.home')
@push('style')
<link rel="stylesheet" href="{{ asset('admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush
@section('title')
Tambah Surat Kelahiran
@endsection
@section('content')
<label class="col-md-6">
  <form action="{{ route('kelahiran.store') }}" method="post">
    @csrf
    <div class="card-body">
      <a href="{{ route('penduduk.create') }}" class="btn btn-sm btn-primary pull-right">Tambah
        Penduduk</a><br /><br />

      <div class="form-group">
        <div class="form-group">
          <label>NIK Ayah</label>
          <select name="nik_ayah" class="form-control" required>
            <option value="" selected disabled>- pilih -</option>
            @foreach ($penduduk as $item)
            <option value="{{ $item->id }}" {{ old('nik_ayah')==$item->nik ? 'selected' : null }}>{{ $item->nik }} -
              {{ $item->name }}

            </option>
            @endforeach
        </div>
        </select>
      </div>
      <div class="form-group">
        <div class="form-group">
          <label>NIK Ibu</label>
          <select name="nik_ibu" class="form-control" required>
            <option value="" selected disabled>- pilih -</option>
            @foreach ($penduduk as $item)
            <option value="{{ $item->id }}" {{ old('nik_ibu')==$item->nik ? 'selected' : null }}>{{ $item->nik }} -
              {{ $item->name }}

            </option>
            @endforeach
        </div>
        </select>
      </div>

      <div class="form-group">
        <label>Nama Anak</label>
        <input type="text" name="nama" class="form-control" placeholder="Nama Anak" required />
      </div>
      <div class="form-group">
        <label>Jenis Kelamin</label>
        <select name="jk" class="form-control" required>
          <option value="" selected disabled>- pilih -</option>
          <option value="Laki Laki">Laki Laki</option>
          <option value="Perempuan">Perempuan</option>
        </select>
      </div>
      <div class="form-group">
        <label>Tempat Tanggal Lahir</label>
        <div class="row">
          <div class="col-xs-4">
            <input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat">
          </div>
          <div class="col-xs-5">
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="date" name="tgl_lahir" class="form-control pull-right">
            </div>
          </div>
        </div>
      </div>
      <div class="bootstrap-timepicker">
        <div class="form-group">
          <label>Pukul</label>
          <div class="input-group">
            <input type="time" name="jam" id="pukul" class="form-control timepicker" required>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="form-group">
          <label>Tanda Tangan</label>
          <select name="petugas_id" class="form-control" required>
            <option value="" selected disabled>- pilih -</option>
            @foreach ($petugas as $item)
            <option value="{{ $item->id }}" {{ old('petugas_id')==$item->id ? 'selected' : null }}>{{ $item->nama }}
            </option>
            @endforeach
        </div>
        </select>
      </div>
      <div class="form-group">
        <input type="submit" class="btn btn-success">
        <a href="" class="btn btn-danger">Batal</a>
      </div>
    </div>
  </form>
</label>
@endsection