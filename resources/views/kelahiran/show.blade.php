<style type="text/css">
  @media print and (width: 21cm) and (height: 33cm) {
    @page {
      margin: 1cm;
    }
  }

  .tabelku {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 2px;
  }
</style>
<style type="text/css" media="print">
  @page {
    size: portrait;
  }
</style>
<table width="100%">
  <tr>
    <td width="15%">Desa/Kelurahan</td>
    <td width="1%">:</td>
    <td width="84%">WARUNGBAMBU</td>
  </tr>
  <tr>
    <td>Kecamatan</td>
    <td>:</td>
    <td>KARAWANG TIMUR</td>
  </tr>
  <tr>
    <td>Kabupaten</td>
    <td>:</td>
    <td>KARAWANG</td>
  </tr>
  <tr>
    <td>Provinsi</td>
    <td>:</td>
    <td>JAWA BARAT</td>
  </tr>
</table>
<br /><br />
<center><b>UNTUK YANG BERSANGKUTAN</b> <br />
  <font size="5"><u><b>SURAT KETERANGAN KELAHIRAN</b></u></font><br />Nomor:
  474.1/{{ $kelahiran->id }}/Desa/ {{ substr($kelahiran->created_at, 10, 14) }}
</center>
<br /><br /><br />
<font align="justify">
  Yang bertandatangan di bawah ini, Kepala Desa Warungbambu Kecamatan
  Karawang Timur Kabupaten Karawang, menerangkan bahwa pada:
</font>
<table width="100%">
  <tr>
    <td width="20%"></td>
    <td width="3%"></td>
    <td width="77%"> </td>
  </tr>
  <tr>
    <td>Tempat, Tanggal Lahir</td>
    <td>:</td>
    <td> {{ $kelahiran->tempat_lahir }} / {{ date('d F Y', strtotime($kelahiran->tgl_lahir)) }}
    </td>
  </tr>
  <tr>
    <td>Pukul</td>
    <td>:</td>
    <td>{{ $kelahiran->jam }}</td>
  </tr>
</table>
<br />
<center>Telah lahir seorang anak Bernama
  :<br /><b>"{{ $kelahiran->nama }}"</b><br /></center>
Dari seorang Ibu :
<table width="100%">
  <tr>
    <td width="20%">Nama Lengkap</td>
    <td width="3%">:</td>
    <td width="77%">
      @foreach ($kelahiran->pendudukk as $item)
      {{ $item->name }}
      @endforeach
    </td>
  </tr>
  <tr>
    <td>Pekerjaan</td>
    <td>:</td>
    <td>@foreach ($kelahiran->pendudukk as $item)
      {{ $item->pekerjaan }}
      @endforeach</td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td>@foreach ($kelahiran->pendudukk as $item)
      {{ $item->alamat }}
      @endforeach</td>
  </tr>
</table> <br>
Dari seorang Ayah <br />
<table width="100%">
  <tr>
    <td width="20%">Nama Lengkap</td>
    <td width="3%">:</td>
    <td width="77%">@foreach ($kelahiran->penduduk as $item)
      {{ $item->name }}
      @endforeach</td>
  </tr>
  <tr>
    <td>Pekerjaan</td>
    <td>:</td>
    <td>@foreach ($kelahiran->penduduk as $item)
      {{ $item->pekerjaan }}
      @endforeach</td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td>@foreach ($kelahiran->penduduk as $item)
      {{ $item->alamat }}
      @endforeach</td>
  </tr>
</table>

<br />
<font align="justify">
  Surat Keterangan ini di gunakan sebagaimana mestinya.<br /><br /><br />
</font>
<table width="100%">
  <tr>
    <td width="50%"></td>
    <td width="50%">
      <center>Warungbambu, {{ $kelahiran->created_at }} </center>
    </td>
  </tr>
  <tr>
    <td></td>
    <td>
      <center>Kepala Desa Warungbambu</center>
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>
      <center><b><u>@foreach ($kelahiran->petugas as $item)
            {{ $item->nama }}
            @endforeach</u></b></center>
    </td>
  </tr>
  <tr>
    <td></td>
    <td>
      <center>NIP.@foreach ($kelahiran->petugas as $item)
        {{ $item->nip }}
        @endforeach </center>
    </td>
  </tr>
</table>
<script>
  window.print();
</script>