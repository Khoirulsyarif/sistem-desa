@extends('layout.home')
@push('style')
<link rel="stylesheet" href="{{ asset('admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush
@section('title')
Tambah Petugas
@endsection
@section('content')
<label class="col-md-6">
  <form action="{{ route('petugas.store') }}" method="post">
    @csrf
    <div class="form-group">
      <label>NIP</label>
      <input type="text" name="nip" class="form-control" required />
    </div>
    <div class="form-group">
      <label>Nama Petugas</label>
      <input type="text" name="nama" class="form-control" required />
    </div>
    <div class="form-group">
      <label>Jabatan</label>
      <input type="text" name="jabatan" class="form-control" required />
    </div>
    <div class="form-group">
      <button type="submit" class="btn btn-primary">Simpan</button>
      <a href="" class="btn btn-danger">Batal</a>
    </div>
  </form>
</label>
@endsection